module.exports = {
    devServer: {
        host: 'localhost'
    },
    transpileDependencies: [
        'vuetify'
    ],
    chainWebpack: (config) => {
        config.module
            .rule('images')
            .use('url-loader')
            .loader('url-loader')
            .tap((options) => {
                options.fallback.options.name = 'img/[name].[ext]'
                return options
            })
    }
}
