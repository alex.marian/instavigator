module.exports = {
    root: true,
    env: {
        node: true,
        jquery: true,
    },
    // extends: ['plugin:vue/essential', '@vue/prettier'],
    extends: ['plugin:vue/recommended', 'eslint:recommended'],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        indent: ['error', 4],
        'sort-imports': [
            'error',
            {
                ignoreCase: true,
                ignoreDeclarationSort: false,
                ignoreMemberSort: false,
                memberSyntaxSortOrder: ['none', 'all', 'single', 'multiple'],
            },
        ],
        'no-alert': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-empty-function':
            process.env.NODE_ENV === 'production' ? 'error' : 'off',
        quotes: ['error', 'single'],
        'space-before-blocks': ['error', 'always'],
        'vue/attribute-hyphenation': ['error', 'always'],
        'vue/html-closing-bracket-spacing': [
            'error',
            {
                startTag: 'never',
                endTag: 'never',
                selfClosingTag: 'never',
            },
        ],
        'vue/html-closing-bracket-newline': [
            'error',
            {
                singleline: 'never',
                multiline: 'never',
            },
        ],
        'vue/max-attributes-per-line': [
            'error',
            {
                singleline: 2,
                multiline: {
                    max: 1,
                    allowFirstLine: true,
                },
            },
        ],
        'arrow-spacing': 'error',
        'vue/v-on-function-call': ['error', 'never'],
        'vue/match-component-file-name': [
            'error',
            {
                extensions: ['jsx', 'vue'],
                shouldMatchCase: true,
            },
        ],
        'block-spacing': ['error', 'always'],
        'vue/block-spacing': ['error', 'always'],
        'vue/no-v-html': 'off',
        'vue/html-indent': ['error', 4]
    },
    globals: {
        _ : false
    },
    parserOptions: {
        parser: 'babel-eslint',
    },
    overrides: [
        {
            files: ['**/__tests__/*.{j,t}s?(x)'],
            env: {
                jest: true,
            },
        },
    ],
};
