import Checkout from '../views/Checkout'
import Contact from '../views/Contact'
import Home from '../views/Home.vue'
import HowItWorks from '../views/HowItWorks'
import NotFound from '../views/NotFound'
import Pricing from '../views/Pricing'
import PrivacyPolicy from '../views/PrivacyPolicy'
import TermsAndConditions from '../views/TermsAndConditions'
import ThankYou from '../views/ThankYou'
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
/* eslint-disable */
const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
    },
    {
        path: '/checkout',
        name: 'Checkout',
        component: Checkout
    },
    {
        path: '/contact',
        name: 'Contact',
        component: Contact
    },
    {
        path: '/thank-you',
        name: 'ThankYou',
        component: ThankYou,
    },
    {
        path: '/how-it-works',
        name: 'HowItWorks',
        component: HowItWorks
    },
    {
        path: '/pricing',
        name: 'Pricing',
        component: Pricing
    },
    {
        path: '/privacy-policy',
        name: 'Policy',
        component: PrivacyPolicy
    },
    {
        path: '/terms-and-conditions',
        name: 'Terms',
        component: TermsAndConditions
    },
    {
        path: '*',
        name:'404',
        component: NotFound
    }

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    window.scrollTo(0, 0)
    next()
})

export default router
