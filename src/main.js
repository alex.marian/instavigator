import App from './App.vue'
import config from '../config'
import materialDesign from 'material-design-icons-iconfont'
import router from './router'
import store from './store'
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'
import VuePageTransition from 'vue-page-transition'
import vuetify from './plugins/vuetify'
import VueTyperPlugin from 'vue-typer'
/* eslint-disable */
Vue.config.productionTip = false

Vue.use(VuePageTransition, {
    router
})
Vue.use(VueAnalytics, {
    id: config.googleAnalyticsId,
    router
})
Vue.use(VueTyperPlugin)
Vue.use(VueAnalytics)
new Vue({
    Materialdesign: materialDesign,
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app')
