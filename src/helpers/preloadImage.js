export const preloadImage = (url) => {
    let img = new Image();
    if(img.width !== 0) {
        img.src= url;
    }
}

export const preloadHowItWorksPageImages = () => {
    preloadImage('../../img/audience.png')
    preloadImage('../../img/growth.png')
    preloadImage('../../img/no_bots_icon.png')
    preloadImage('../../img/real_growth.png')
    preloadImage('../../img/smart_targeting.png')
    preloadImage('../../img/social_influence.png')
    preloadImage('../../img/support.png')
    preloadImage('../../img/statistics.png')
    preloadImage('../../img/consistenly_growing.png')
    preloadImage('../../img/grow_audience.jpg')
    preloadImage('../../img/results.png')
    preloadImage('../../img/insta-gif.gif')
    preloadImage(
        'https://www.fillmurray.com/640/360')
}

export const preloadHomePageImages = () => {
    preloadImage('../../img/audience.png')
    preloadImage('../../img/insta-followers-increase.png')
    preloadImage('../../img/setup.png')
    preloadImage('../../img/support.png')
    preloadImage('../../img/iphone.png')
    preloadImage('../../img/real_growth.png')
}
