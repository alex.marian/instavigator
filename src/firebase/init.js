import firebase from 'firebase'
import firestore from 'firebase/firestore' // eslint-disable-line

const firebaseConfig = {
    apiKey: 'AIzaSyDnSHphxRcB0iVgDO6DSwskLcmCKr8BR-4',
    authDomain: 'instavigator-2cb33.firebaseapp.com',
    databaseURL: 'https://instavigator-2cb33.firebaseio.com',
    projectId: 'instavigator-2cb33',
    storageBucket: 'instavigator-2cb33.appspot.com',
    messagingSenderId: '313250467122',
    appId: '1:313250467122:web:8462b9e34ee12361669b2b',
    measurementId: 'G-KNYKBVJ10C'
};

const firebaseApp = firebase.initializeApp(firebaseConfig)

// Export firestore database
export default firebaseApp.firestore()
