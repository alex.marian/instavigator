import axios from 'axios'
import config from '../../../config/index';

const state = {
    clientInfo: null,
    error: {
        hasError: false,
        errorMessage: ''
    },
}

const getters = {
    getContactClientInfo: state => state.clientInfo,
    getContactClientInfoError: state => state.error,
}

const mutations = {
    RECEIVE_CONTACT_CLIENT_INFO (state, info) {
        state.clientInfo = info;
    },
    RECEIVE_CONTACT_CLIENT_INFO_ERROR (state, error) {
        state.error.hasError = error;
    },
    RECEIVE_CONTACT_CLIENT_INFO_ERROR_MESSAGE (state, errorMessage) {
        state.error.errorMessage = errorMessage;
    }
}

const actions = {
    sendClientMessage
    (
        {commit},
        info,
        url = `${config.contactApiUrl}`
    )
    {
        return axios.post(url, info).then(response => {
            commit('RECEIVE_CONTACT_CLIENT_INFO', response.data);
        }).catch(error => {
            let defaultMessage = 'Unable to process the request'
            if(error.response && error.response.data.errors && error.response.data.errors !== '') {
                commit('RECEIVE_CONTACT_CLIENT_INFO_ERROR', true)
                commit('RECEIVE_CONTACT_CLIENT_INFO_ERROR_MESSAGE', error.response.data.errors)
            } else {
                commit('RECEIVE_CONTACT_CLIENT_INFO_ERROR', true);
                commit('RECEIVE_CONTACT_CLIENT_INFO_ERROR_MESSAGE', defaultMessage)

            }
        });
    },
    resetContactState({commit}) {
        commit('RECEIVE_CONTACT_CLIENT_INFO_ERROR', false)
        commit('RECEIVE_CONTACT_CLIENT_INFO_ERROR_MESSAGE', '')
    }
}
export default {
    state,
    getters,
    mutations,
    actions
}
