const state = {
    language: 'en',
    captchaLoaded: false,
    captchaPassed: false,
}

const getters = {
    captchaLanguage: state => state.language,
    getCaptchaLoaded: state => state.captchaLoaded,
    getCaptchaPassedValue: state => state.captchaPassed
}

const mutations = {
    SET_CAPTCHA_LOADED_VALUE: (state, captchaLoaded) => {
        state.captchaLoaded = captchaLoaded;
    },
    SET_CAPTCHA_PASSED_VALUE: (state, captchaPassed) => {
        state.captchaPassed = captchaPassed;
    },
}

const actions = {
    captchaLoaded({ commit }, captchaLoaded) {
        commit('SET_CAPTCHA_LOADED_VALUE', captchaLoaded);
    },
    captchaPassed({ commit }, captchaPassed) {
        commit('SET_CAPTCHA_PASSED_VALUE', captchaPassed);
    }
}
export default {
    state,
    getters,
    mutations,
    actions
}
