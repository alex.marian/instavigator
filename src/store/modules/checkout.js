/* esling-disable */
import axios from 'axios'
import config from '../../../config/index';
const state = {
    clientInfo: null,
    error: {
        hasError: false,
        errorMessage: ''
    },
    pricingPlanSelected: null,
    thankYouPage: false
}

const getters = {
    getClientInfo: state => state.clientInfo,
    getClientInfoError: state => state.error,
    getPricePlanSelected: state => state.pricingPlanSelected,
    getThankYouPageValue: state => state.thankYouPage
}

const mutations = {
    GET_PRICE_SELECTED (state, pricePlan) {
        state.pricingPlanSelected = pricePlan
    },
    RECEIVE_CLIENT_INFO (state, info) {
        state.clientInfo = info;
    },
    RECEIVE_CLIENT_INFO_ERROR (state, error) {
        state.error.hasError = error;
    },
    RECEIVE_CLIENT_INFO_ERROR_MESSAGE (state, errorMessage) {
        state.error.errorMessage = errorMessage;
    },
    RECEIVE_THANK_YOU_PAGE_VALUE (state, thankYouPageValue) {
        state.thankYouPage = thankYouPageValue
    }
}

const actions = {
    updatePricingPlan ({ commit }, pricePlan) {
        commit('GET_PRICE_SELECTED', pricePlan)
    },
    sendClientInfo
    (
        {commit},
        info,
        url = `${config.checkoutApiUrl}`
    )
    {
        return axios.post(url, info).then(response => {
            commit('RECEIVE_CLIENT_INFO', response.data);
        }).catch(error => {
            let defaultMessage = 'Unable to process the request'
            if(error.response && error.response.data.errors && error.response.data.errors !== '') {
                commit('RECEIVE_CLIENT_INFO_ERROR', true)
                commit('RECEIVE_CLIENT_INFO_ERROR_MESSAGE', error.response.data.errors)
            } else {
                commit('RECEIVE_CLIENT_INFO_ERROR', true);
                commit('RECEIVE_CLIENT_INFO_ERROR_MESSAGE', defaultMessage)

            }
        });
    },
    goToThankYouPage({commit}, thanYouPage) {
        commit('RECEIVE_THANK_YOU_PAGE_VALUE', thanYouPage)
    },
    resetState({commit}) {
        commit('RECEIVE_CLIENT_INFO_ERROR', false)
        commit('RECEIVE_CLIENT_INFO_ERROR_MESSAGE', '')
    }
}
export default {
    state,
    getters,
    mutations,
    actions
}
