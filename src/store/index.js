import Captcha from './modules/captcha'
import Checkout from './modules/checkout'
import Contact from './modules/contact'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        Captcha,
        Checkout,
        Contact
    }
})
