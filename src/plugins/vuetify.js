import 'font-awesome/css/font-awesome.min.css'
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

const opts = {
    theme: {
        primary: '#3474ff',
        secondary: '#11ca79'
    },
    icons: {
        iconfont: 'mdi',
    }
}

export default new Vuetify(opts)
